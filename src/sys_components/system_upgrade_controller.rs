use anyhow::{anyhow, Result};
use kube::{Api, Client};
use semver::Version;

use crate::kubernetes::apply::apply_any;
use crate::sys_components::shared::{
    download_and_parse_release_asset, get_latest_release_from_repo_semver,
};

pub async fn get_latest_release(installed_version: Version) -> Result<Version> {
    get_latest_release_from_repo_semver(
        Some(installed_version),
        "rancher".to_string(),
        "system-upgrade-controller".to_string(),
    )
    .await
}

pub async fn get_installed_version(client: Client) -> Result<Version> {
    let api: Api<k8s_openapi::api::apps::v1::Deployment> =
        Api::namespaced(client, "system-upgrade");
    let spec = api
        .get("system-upgrade-controller")
        .await?
        .spec
        .ok_or_else(|| anyhow!("Failed to get deployment spec!"))?;
    let pod_spec = spec
        .template
        .spec
        .ok_or_else(|| anyhow!("Failed to get deployment template spec!"))?;
    let container = pod_spec
        .containers
        .into_iter()
        .find(|container| {
            container
                .image
                .as_ref()
                .is_some_and(|image| image.contains("system-upgrade-controller"))
        })
        .ok_or_else(|| anyhow!("Failed to get installed version!"))?;
    let image = container
        .image
        .ok_or_else(|| anyhow!("Failed to get installed version!"))?;
    let installed_version = image
        .split(':')
        .last()
        .ok_or_else(|| anyhow!("Failed to get installed version!"))?;

    Ok(Version::parse(installed_version.trim_start_matches('v'))?)
}

pub async fn get_available_update(client: &Client) -> Result<Option<Version>> {
    let installed_version = get_installed_version(client.clone()).await?;
    let latest_version = get_latest_release(installed_version.clone()).await?;
    if latest_version > installed_version {
        Ok(Some(latest_version))
    } else {
        Ok(None)
    }
}

pub async fn upgrade(client: &Client, version: &Version) -> Result<()> {
    let latest_config = download_and_parse_release_asset(
        "rancher",
        "system-upgrade-controller",
        &format!("v{}.{}.{}", version.major, version.minor, version.patch),
        "system-upgrade-controller.yaml",
    )
    .await?;
    apply_any(client.clone(), latest_config).await?;
    let latest_crd = download_and_parse_release_asset(
        "rancher",
        "system-upgrade-controller",
        &format!("v{}.{}.{}", version.major, version.minor, version.patch),
        "crd.yaml",
    )
    .await?;
    apply_any(client.clone(), latest_crd).await?;
    Ok(())
}
