use anyhow::Result;
use kube::Client;
use semver::Version;

use crate::kubernetes::apply::apply_any;
use crate::sys_components::shared::{
    download_and_parse_release_asset, get_installed_version_for_deployment,
    get_latest_release_from_repo_semver,
};

pub async fn get_latest_release(installed_version: Version) -> Result<Version> {
    get_latest_release_from_repo_semver(
        Some(installed_version),
        "cert-manager".to_string(),
        "cert-manager".to_string(),
    )
    .await
}

pub async fn get_installed_version(client: Client) -> Result<Version> {
    get_installed_version_for_deployment(client, "cert-manager", "cert-manager").await
}

pub async fn get_available_update(client: &Client) -> Result<Option<Version>> {
    let installed_version = get_installed_version(client.clone()).await?;
    let latest_version = get_latest_release(installed_version.clone()).await?;
    if latest_version > installed_version {
        Ok(Some(latest_version))
    } else {
        Ok(None)
    }
}

pub async fn upgrade(client: &Client, version: &Version) -> Result<()> {
    let latest_config = download_and_parse_release_asset(
        "cert-manager",
        "cert-manager",
        &format!("v{}.{}.{}", version.major, version.minor, version.patch),
        "cert-manager.yaml",
    )
    .await?;
    apply_any(client.clone(), latest_config).await?;
    Ok(())
}
