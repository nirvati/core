use std::fmt::Display;

use anyhow::{anyhow, bail, Result};
use itertools::Itertools;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use kube::api::PostParams;
use kube::Client;
use semver::Version;
use slugify::slugify;
use tokio::try_join;

use k8s_crds_system_upgrade_controller::plans::*;

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug, Clone)]
pub struct K3sVersion {
    pub k8s: Version,
    pub k3s: u8,
}

impl std::str::FromStr for K3sVersion {
    type Err = anyhow::Error;

    // Format is something like v1.28.3+k3s2
    // <k8s version>+k3s<k3s version>
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut split = s.split("+k3s");
        let k8s_version = split
            .next()
            .ok_or_else(|| anyhow!("Failed to get k8s version!"))?;
        let k3s_version = split
            .next()
            .ok_or_else(|| anyhow!("Failed to get k3s version!"))?;
        let k8s_version = Version::parse(k8s_version.trim_start_matches('v'))?;
        let k3s_version = k3s_version.parse()?;
        Ok(Self {
            k8s: k8s_version,
            k3s: k3s_version,
        })
    }
}

impl Display for K3sVersion {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let str = format!(
            "v{}.{}.{}+k3s{}",
            self.k8s.major, self.k8s.minor, self.k8s.patch, self.k3s
        );
        write!(f, "{}", str)
    }
}

pub async fn get_latest_release_from_repo(
    installed_version: K3sVersion,
    owner: String,
    repo: String,
) -> Result<K3sVersion> {
    let octocrab = octocrab::instance();
    let mut page: u32 = 1;
    while page < 11 {
        let releases = octocrab
            .repos(&owner, &repo)
            .releases()
            .list()
            .page(page)
            .per_page(20)
            .send()
            .await?;
        // Filter the releases to only include the ones that are newer than the currently installed version,
        // And still have the same major version
        let found_latest_release = releases
            .items
            .iter()
            .filter_map(|release| {
                let tag = release.tag_name.as_str().trim_start_matches('v');
                let release_version: K3sVersion = tag.parse().ok()?;
                if release_version.k8s.major == installed_version.k8s.major
                    // https://kubernetes.io/releases/version-skew-policy/
                    && (release_version.k8s.minor == installed_version.k8s.minor || release_version.k8s.minor == installed_version.k8s.minor + 1)
                    && release_version >= installed_version
                    && !release.prerelease
                {
                    Some(release_version)
                } else {
                    None
                }
            })
            .sorted()
            .last();
        if let Some(release) = found_latest_release {
            return Ok(release);
        } else if releases.items.len() < 20 {
            bail!("Failed to find latest release!");
        } else {
            page += 1;
        }
    }
    bail!("Failed to find latest release!");
}

pub async fn get_installed_version(client: &Client) -> Result<K3sVersion> {
    let version = client.apiserver_version().await?;
    if !version.git_version.contains("k3s") {
        bail!("Not running k3s!");
    }
    version.git_version.parse()
}

pub async fn is_k3s(client: &Client) -> Result<bool> {
    let version = client.apiserver_version().await?;
    Ok(version.git_version.contains("k3s"))
}

pub async fn get_latest_release(installed_version: &K3sVersion) -> Result<K3sVersion> {
    let latest_version = get_latest_release_from_repo(
        installed_version.clone(),
        "k3s-io".to_string(),
        "k3s".to_string(),
    )
    .await?;
    Ok(latest_version)
}

pub async fn get_upgrade_plan(version: &K3sVersion) -> [Plan; 2] {
    [
        Plan {
            metadata: k8s_meta::ObjectMeta {
                name: Some(format!(
                    "k3s-server-upgrade-{}",
                    slugify!(&version.to_string())
                )),
                ..Default::default()
            },
            spec: PlanSpec {
                channel: None,
                concurrency: Some(1),
                cordon: Some(true),
                drain: None,
                node_selector: Some(PlanNodeSelector {
                    match_expressions: Some(vec![PlanNodeSelectorMatchExpressions {
                        key: Some("node-role.kubernetes.io/control-plane".to_string()),
                        operator: Some("In".to_string()),
                        values: Some(vec!["true".to_string()]),
                    }]),
                    ..Default::default()
                }),
                prepare: None,
                secrets: None,
                service_account_name: Some("system-upgrade".to_string()),
                tolerations: None,
                upgrade: PlanUpgrade {
                    image: Some("rancher/k3s-upgrade".to_string()),
                    ..Default::default()
                },
                version: Some(version.to_string()),
            },
            status: None,
        },
        Plan {
            metadata: k8s_meta::ObjectMeta {
                name: Some(format!(
                    "k3s-agent-upgrade-{}",
                    slugify!(&version.to_string())
                )),
                ..Default::default()
            },
            spec: PlanSpec {
                channel: None,
                concurrency: Some(1),
                cordon: Some(true),
                drain: None,
                node_selector: Some(PlanNodeSelector {
                    match_expressions: Some(vec![PlanNodeSelectorMatchExpressions {
                        key: Some("node-role.kubernetes.io/control-plane".to_string()),
                        operator: Some("DoesNotExist".to_string()),
                        ..Default::default()
                    }]),
                    ..Default::default()
                }),
                prepare: None,
                secrets: None,
                service_account_name: Some("system-upgrade".to_string()),
                tolerations: None,
                upgrade: PlanUpgrade {
                    image: Some("rancher/k3s-upgrade".to_string()),
                    ..Default::default()
                },
                version: Some(version.to_string()),
            },
            status: None,
        },
    ]
}

pub async fn upgrade(client: &Client, version: &K3sVersion) -> Result<()> {
    let plan = get_upgrade_plan(version).await;
    let api: kube::Api<Plan> = kube::Api::namespaced(client.clone(), "system-upgrade");
    let pp = PostParams::default();
    try_join!(api.create(&pp, &plan[0]), api.create(&pp, &plan[1]))?;
    Ok(())
}
