use kube::Client;

pub mod upgrades;
pub mod tailscale;
pub mod entropy;
pub mod node;
pub mod middlewares;
pub mod users;
pub mod services;
pub mod proxies;


pub mod api {
    tonic::include_proto!("nirvati");
}

#[derive(Clone)]
pub struct ApiServer {
    pub kube_client: Client,
    pub nirvati_seed: String,
    pub tailscale_socket: String,
}

#[macro_export]
macro_rules! expect {
    ($expr:expr, $msg:expr) => {
        $expr.map_err(|err| {
            tracing::error!("{}: {:#}", $msg, err);
            Status::internal($msg)
        })
    };
    ($expr:expr, $msg:expr, $type:path) => {
        $expr.map_err(|err| {
            tracing::error!("{}: {:#}", $msg, err);
            $type($msg)
        })
    };
}
