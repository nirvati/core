use tonic::{async_trait, Request, Response, Status};
use crate::expect;
use crate::grpc::api::{AddUserNsRequest, DeleteUserNsRequest, Empty};
use crate::grpc::api::users_server::Users;
use crate::grpc::ApiServer;

#[async_trait]
impl Users for ApiServer {
    async fn create_user_namespace(&self, request: Request<AddUserNsRequest>) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        expect!(crate::manage::kubernetes::create_namespace(self.kube_client.clone(), &req.user).await, "Failed to create user namespace")?;
        Ok(Response::new(Empty {}))
    }

    async fn delete_user_namespace(&self, request: Request<DeleteUserNsRequest>) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        expect!(crate::manage::kubernetes::delete_namespace(self.kube_client.clone(), &req.user).await, "Failed to delete user namespace")?;
        Ok(Response::new(Empty {}))
    }
}
