use crate::grpc::api::entropy_server::Entropy;
use crate::grpc::api::{EntropyRequest, EntropyResponse};
use crate::grpc::ApiServer;
use tonic::{async_trait, Request, Response, Status};

#[async_trait]
impl Entropy for ApiServer {
    async fn derive_entropy(
        &self,
        request: Request<EntropyRequest>,
    ) -> Result<Response<EntropyResponse>, Status> {
        let req = request.into_inner();
        let entropy = crate::utils::derive_entropy(&req.input, &self.nirvati_seed);
        Ok(Response::new(EntropyResponse { entropy }))
    }
}
