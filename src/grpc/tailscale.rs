use std::sync::OnceLock;

use tailscale_localapi::{LocalApi, UnixStreamClient};
use tonic::{async_trait, Request, Response, Status};

use crate::expect;
use crate::grpc::api::tailscale_server::Tailscale;
use crate::grpc::api::{Empty, TailscaleStatus};

use super::ApiServer;

static TAILSCALE_SOCKET: OnceLock<LocalApi<UnixStreamClient>> = OnceLock::new();

#[async_trait]
impl Tailscale for ApiServer {
    async fn get_status(
        &self,
        _request: Request<Empty>,
    ) -> Result<Response<TailscaleStatus>, Status> {
        let client =
            TAILSCALE_SOCKET.get_or_init(|| LocalApi::new_with_socket_path(&self.tailscale_socket));
        let response = expect!(client.status().await, "Failed to get tailscale status")?;
        Ok(Response::new(TailscaleStatus {
            auth_url: response.auth_url,
            authenticated: response.current_tailnet.is_some(),
            own_ip: response
                .self_status
                .tailscale_ips
                .first()
                .map(|ip| ip.to_string()),
        }))
    }

    async fn start(&self, _request: Request<Empty>) -> Result<Response<Empty>, Status> {
        let client =
            TAILSCALE_SOCKET.get_or_init(|| LocalApi::new_with_socket_path(&self.tailscale_socket));
        expect!(client.start().await, "Failed to start tailscale")?;
        Ok(Response::new(Empty {}))
    }

    async fn start_login(&self, _request: Request<Empty>) -> Result<Response<Empty>, Status> {
        let client =
            TAILSCALE_SOCKET.get_or_init(|| LocalApi::new_with_socket_path(&self.tailscale_socket));
        expect!(client.login_interactive().await, "Failed to start tailscale")?;
        Ok(Response::new(Empty {}))
    }
}
