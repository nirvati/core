use tonic::{async_trait, Request, Response, Status};

use super::api::middlewares_server::Middlewares;
use crate::manage;

use super::api::{AddUserAuthRequest, DeleteUserAuthRequest, Empty};
use super::ApiServer;

#[async_trait]
impl Middlewares for ApiServer {
    async fn add_user_auth(
        &self,
        request: Request<AddUserAuthRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        manage::kubernetes::generate_auth_middleware(&self.kube_client, &req.user, &req.password)
            .await
            .map_err(|err| {
                tracing::error!("Failed to add user auth: {:#}", err);
                Status::internal("Failed to add user auth")
            })?;
        Ok(Response::new(Empty {}))
    }

    async fn remove_user_auth(
        &self,
        request: Request<DeleteUserAuthRequest>,
    ) -> Result<Response<Empty>, Status> {
        let req = request.into_inner();
        manage::kubernetes::delete_auth_middleware(&self.kube_client, &req.user)
            .await
            .map_err(|err| {
                tracing::error!("Failed to remove user auth: {:#}", err);
                Status::internal("Failed to remove user auth")
            })?;
        Ok(Response::new(Empty {}))
    }
}
