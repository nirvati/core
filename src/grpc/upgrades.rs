use tonic::{async_trait, Request, Response, Status};

use crate::expect;
use crate::grpc::api::upgrades_server::Upgrades;
use crate::grpc::api::{Empty, K3sVersion, VersionInfo};

use super::ApiServer;

#[async_trait]
impl Upgrades for ApiServer {
    async fn get_longhorn_version(
        &self,
        _request: Request<Empty>,
    ) -> Result<Response<VersionInfo>, Status> {
        let installed = expect!(
            crate::sys_components::longhorn::get_installed_version(self.kube_client.clone()).await,
            "Failed to get Longhorn version"
        )?;
        Ok(Response::new(VersionInfo {
            version: installed.to_string(),
        }))
    }

    async fn get_latest_longhorn_version(
        &self,
        _request: Request<Empty>,
    ) -> Result<Response<VersionInfo>, Status> {
        let installed = expect!(
            crate::sys_components::longhorn::get_installed_version(self.kube_client.clone()).await,
            "Failed to get installed Longhorn version"
        )?;
        let latest = expect!(
            crate::sys_components::longhorn::get_latest_release(installed).await,
            "Failed to get latest Longhorn version"
        )?;
        Ok(Response::new(VersionInfo {
            version: latest.to_string(),
        }))
    }

    async fn update_longhorn(
        &self,
        request: Request<VersionInfo>,
    ) -> Result<Response<Empty>, Status> {
        let version_to_install = expect!(
            request.into_inner().version.parse(),
            "Failed to parse version",
            Status::invalid_argument
        )?;
        expect!(
            crate::sys_components::longhorn::upgrade(&self.kube_client, &version_to_install).await,
            "Failed to install update!"
        )?;
        Ok(Response::new(Empty {}))
    }

    async fn get_cert_manager_version(
        &self,
        _request: Request<Empty>,
    ) -> Result<Response<VersionInfo>, Status> {
        let installed = expect!(
            crate::sys_components::cert_manager::get_installed_version(self.kube_client.clone())
                .await,
            "Failed to get cert-manager version"
        )?;
        Ok(Response::new(VersionInfo {
            version: installed.to_string(),
        }))
    }

    async fn get_latest_cert_manager_version(
        &self,
        _request: Request<Empty>,
    ) -> Result<Response<VersionInfo>, Status> {
        let installed = expect!(
            crate::sys_components::cert_manager::get_installed_version(self.kube_client.clone())
                .await,
            "Failed to get installed cert-manager version"
        )?;
        let latest = expect!(
            crate::sys_components::cert_manager::get_latest_release(installed).await,
            "Failed to get latest cert-manager version"
        )?;
        Ok(Response::new(VersionInfo {
            version: latest.to_string(),
        }))
    }

    async fn update_cert_manager(
        &self,
        request: Request<VersionInfo>,
    ) -> Result<Response<Empty>, Status> {
        let version_to_install = expect!(
            request.into_inner().version.parse(),
            "Failed to parse version",
            Status::invalid_argument
        )?;
        expect!(
            crate::sys_components::cert_manager::upgrade(&self.kube_client, &version_to_install)
                .await,
            "Failed to install update!"
        )?;
        Ok(Response::new(Empty {}))
    }

    async fn get_k3s_version(
        &self,
        _request: Request<Empty>,
    ) -> Result<Response<K3sVersion>, Status> {
        let is_installed = expect!(
            crate::sys_components::k3s::is_k3s(&self.kube_client).await,
            "Failed to check if K3s is installed"
        )?;
        Ok(Response::new(K3sVersion {
            installed: is_installed,
            version: if is_installed {
                Some(
                    expect!(
                        crate::sys_components::k3s::get_installed_version(&self.kube_client).await,
                        "Failed to get K3s version"
                    )?
                    .to_string(),
                )
            } else {
                None
            },
        }))
    }

    async fn get_latest_k3s_version(
        &self,
        _request: Request<Empty>,
    ) -> Result<Response<VersionInfo>, Status> {
        let installed = expect!(
            crate::sys_components::k3s::get_installed_version(&self.kube_client).await,
            "Failed to get installed K3s version"
        )?;
        let latest = expect!(
            crate::sys_components::k3s::get_latest_release(&installed).await,
            "Failed to get latest K3s version"
        )?;
        Ok(Response::new(VersionInfo {
            version: latest.to_string(),
        }))
    }

    async fn update_k3s(&self, request: Request<VersionInfo>) -> Result<Response<Empty>, Status> {
        let version_to_install = expect!(
            request.into_inner().version.parse(),
            "Failed to parse version",
            Status::invalid_argument
        )?;
        expect!(
            crate::sys_components::k3s::upgrade(&self.kube_client, &version_to_install).await,
            "Failed to install update!"
        )?;
        Ok(Response::new(Empty {}))
    }

    async fn get_system_upgrade_controller_version(
        &self,
        _request: Request<Empty>,
    ) -> Result<Response<VersionInfo>, Status> {
        let installed = expect!(
            crate::sys_components::system_upgrade_controller::get_installed_version(
                self.kube_client.clone()
            )
            .await,
            "Failed to get system-upgrade-controller version"
        )?;
        Ok(Response::new(VersionInfo {
            version: installed.to_string(),
        }))
    }

    async fn get_latest_system_upgrade_controller_version(
        &self,
        _request: Request<Empty>,
    ) -> Result<Response<VersionInfo>, Status> {
        let installed = expect!(
            crate::sys_components::system_upgrade_controller::get_installed_version(
                self.kube_client.clone()
            )
            .await,
            "Failed to get installed system-upgrade-controller version"
        )?;
        let latest = expect!(
            crate::sys_components::system_upgrade_controller::get_latest_release(installed).await,
            "Failed to get latest system-upgrade-controller version"
        )?;
        Ok(Response::new(VersionInfo {
            version: latest.to_string(),
        }))
    }

    async fn update_system_upgrade_controller(
        &self,
        request: Request<VersionInfo>,
    ) -> Result<Response<Empty>, Status> {
        let version_to_install = expect!(
            request.into_inner().version.parse(),
            "Failed to parse version",
            Status::invalid_argument
        )?;
        expect!(
            crate::sys_components::system_upgrade_controller::upgrade(
                &self.kube_client,
                &version_to_install
            )
            .await,
            "Failed to install update!"
        )?;
        Ok(Response::new(Empty {}))
    }
}
