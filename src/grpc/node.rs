use tonic::{async_trait, Request, Response, Status};
use crate::expect;
use crate::grpc::api::{Empty, NodeInfo};
use crate::grpc::api::node_server::Node;
use crate::grpc::ApiServer;

#[async_trait]
impl Node for ApiServer {
    async fn get_info(&self, _request: Request<Empty>) -> Result<Response<NodeInfo>, Status> {
        let ip = expect!(
            crate::manage::kubernetes::get_node_ip(&self.kube_client).await,
            "Failed to get own IP"
        )?;
        Ok(Response::new(NodeInfo { ip }))
    }
}