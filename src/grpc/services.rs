use crate::expect;
use crate::grpc::api::add_service_request::Remote;
use crate::grpc::api::services_server::Services;
use crate::grpc::api::{AddServiceRequest, Empty, RemoveServiceRequest};
use crate::grpc::ApiServer;
use crate::kubernetes::apply::apply_with_ns;
use crate::kubernetes::services::{
    generate_service_and_endpoint_for_ip, generate_service_for_dns_name,
};
use k8s_openapi::api::core::v1::{Endpoints, Service};
use kube::Api;
use tonic::{async_trait, Request, Response, Status};

#[async_trait]
impl Services for ApiServer {
    async fn add_service(
        &self,
        request: Request<AddServiceRequest>,
    ) -> Result<Response<Empty>, Status> {
        let request = request.into_inner();
        let Some(remote) = request.remote else {
            return Err(Status::invalid_argument("Remote is required"));
        };
        match remote {
            Remote::DnsName(dns_name) => {
                let service =
                    generate_service_for_dns_name(&request.name, &dns_name);
                expect!(
                    apply_with_ns(self.kube_client.clone(), &[service], &request.namespace).await,
                    "Failed to apply service"
                )?;
            }
            Remote::Ip(ip_addr) => {
                let tcp_ports = request
                    .tcp_ports
                    .iter()
                    .map(|port| (*port).try_into())
                    .collect::<Result<Vec<_>, _>>()
                    .map_err(|_| Status::invalid_argument("Invalid port"))?;
                let udp_ports = request
                    .udp_ports
                    .iter()
                    .map(|port| (*port).try_into())
                    .collect::<Result<Vec<_>, _>>()
                    .map_err(|_| Status::invalid_argument("Invalid port"))?;
                let (service, endpoint) = generate_service_and_endpoint_for_ip(
                    &request.name,
                    &ip_addr,
                    &tcp_ports,
                    &udp_ports,
                );
                expect!(
                    apply_with_ns(self.kube_client.clone(), &[service], &request.namespace).await,
                    "Failed to apply service"
                )?;
                expect!(
                    apply_with_ns(self.kube_client.clone(), &[endpoint], &request.namespace).await,
                    "Failed to apply endpoint"
                )?;
            }
        }
        Ok(Response::new(Empty {}))
    }

    async fn remove_service(
        &self,
        request: Request<RemoveServiceRequest>,
    ) -> Result<Response<Empty>, Status> {
        let request = request.into_inner();
        let service_api: Api<Service> =
            Api::namespaced(self.kube_client.clone(), &request.namespace);
        expect!(
            service_api.delete(&request.name, &Default::default()).await,
            "Failed to delete service"
        )?;
        let endpoint_api: Api<Endpoints> =
            Api::namespaced(self.kube_client.clone(), &request.namespace);
        if let Err(err) = endpoint_api
            .delete(&request.name, &Default::default())
            .await
        {
            tracing::debug!("Failed to delete endpoint: {}", err);
        };
        Ok(Response::new(Empty {}))
    }
}
