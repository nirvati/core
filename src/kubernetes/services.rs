use k8s_openapi::api::core::v1 as k8s;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;

pub fn generate_service_for_dns_name(
    service_name: &str,
    dns_name: &str,
) -> k8s::Service {
    k8s::Service {
        metadata: k8s_meta::ObjectMeta {
            name: Some(service_name.to_string()),
            ..Default::default()
        },
        spec: Some(k8s::ServiceSpec {
            type_: Some("ExternalName".to_string()),
            external_name: Some(dns_name.to_string()),
            ..Default::default()
        }),
        status: None,
    }
}

pub fn generate_service_and_endpoint_for_ip(
    service_name: &str,
    ip: &str,
    tcp_ports: &[u16],
    udp_ports: &[u16],
) -> (k8s::Service, k8s::Endpoints) {
    (
        k8s::Service {
            metadata: k8s_meta::ObjectMeta {
                name: Some(service_name.to_string()),
                ..Default::default()
            },
            spec: Some(k8s::ServiceSpec {
                external_name: None,
                ports: Some(
                    tcp_ports
                        .into_iter()
                        .map(|port| k8s::ServicePort {
                            port: *port as i32,
                            name: Some(format!("tcp-{}", port)),
                            ..Default::default()
                        })
                        .chain(udp_ports.into_iter().map(|port| k8s::ServicePort {
                            port: *port as i32,
                            name: Some(format!("udp-{}", port)),
                            protocol: Some("UDP".to_string()),
                            ..Default::default()
                        }))
                        .collect(),
                ),
                type_: Some("".to_string()),
                ..Default::default()
            }),
            status: None,
        },
        k8s::Endpoints {
            metadata: k8s_meta::ObjectMeta {
                name: Some(service_name.to_string()),
                ..Default::default()
            },
            subsets: Some(vec![k8s::EndpointSubset {
                addresses: Some(vec![k8s::EndpointAddress {
                    ip: ip.to_string(),
                    ..Default::default()
                }]),
                ports: Some(
                    tcp_ports
                        .into_iter()
                        .map(|port| k8s::EndpointPort {
                            port: *port as i32,
                            protocol: Some("TCP".to_string()),
                            name: Some(format!("tcp-{}", port)),
                            ..Default::default()
                        })
                        .chain(udp_ports.into_iter().map(|port| k8s::EndpointPort {
                            port: *port as i32,
                            protocol: Some("UDP".to_string()),
                            name: Some(format!("udp-{}", port)),
                            ..Default::default()
                        }))
                        .collect(),
                ),
                ..Default::default()
            }]),
        },
    )
}
