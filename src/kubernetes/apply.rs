use anyhow::{bail, Result};
use k8s_openapi::{ClusterResourceScope, NamespaceResourceScope};
use kube::api::{ApiResource, DynamicObject, GroupVersionKind, Patch, PatchParams};
use kube::discovery::{ApiCapabilities, Scope};
use kube::{Api, Client, Discovery, Resource, ResourceExt};

fn dynamic_api(
    ar: ApiResource,
    caps: ApiCapabilities,
    client: Client,
    ns: Option<&str>,
    all: bool,
) -> Api<DynamicObject> {
    if caps.scope == Scope::Cluster || all {
        Api::all_with(client, &ar)
    } else if let Some(namespace) = ns {
        Api::namespaced_with(client, namespace, &ar)
    } else {
        Api::default_namespaced_with(client, &ar)
    }
}

pub async fn apply_any(client: Client, data: Vec<serde_yaml::Value>) -> Result<()> {
    let discovery = Discovery::new(client.clone()).run().await?;
    let ssapply = PatchParams::apply("nirvati").force();
    for doc in data {
        let obj: DynamicObject = serde_yaml::from_value(doc)?;
        let namespace = obj.metadata.namespace.as_deref();
        let gvk = if let Some(tm) = &obj.types {
            GroupVersionKind::try_from(tm)?
        } else {
            bail!("cannot apply object without valid TypeMeta {:?}", obj);
        };
        let name = obj.name_any();
        if let Some((ar, caps)) = discovery.resolve_gvk(&gvk) {
            let api = dynamic_api(ar, caps, client.clone(), namespace, false);
            tracing::debug!("Applying {}: \n{}", gvk.kind, serde_yaml::to_string(&obj)?);
            let data: serde_json::Value = serde_json::to_value(&obj)?;
            let _r = api.patch(&name, &ssapply, &Patch::Apply(data)).await?;
            tracing::debug!("Applied {} {}", gvk.kind, name);
        } else {
            tracing::warn!("Cannot apply document for unknown {:?}", gvk);
        }
    }
    Ok(())
}

pub async fn apply<
    ResourceType: Resource
        + Clone
        + serde::de::DeserializeOwned
        + std::fmt::Debug
        + k8s_openapi::Metadata
        + serde::Serialize,
>(
    client: Client,
    resources: &[ResourceType],
) -> Result<()>
where
    <ResourceType as kube::Resource>::DynamicType: std::default::Default,
    ResourceType: kube::Resource<Scope = ClusterResourceScope>,
{
    let ssapply = PatchParams::apply("nirvati").force();
    let api: Api<ResourceType> = Api::all(client);
    for resource in resources {
        let name = resource.name_any();
        api.patch(&name, &ssapply, &Patch::Apply(resource)).await?;
        tracing::debug!("Applied {} {}", ResourceType::KIND, name);
    }
    Ok(())
}

pub async fn apply_with_ns<
    ResourceType: Resource
        + Clone
        + serde::de::DeserializeOwned
        + std::fmt::Debug
        + serde::Serialize,
>(
    client: Client,
    resources: &[ResourceType],
    namespace: &str,
) -> Result<()>
where
    <ResourceType as Resource>::DynamicType: Default,
    ResourceType: Resource<Scope = NamespaceResourceScope>,
{
    let ssapply = PatchParams::apply("nirvati").force();
    let api: Api<ResourceType> = Api::namespaced(client, namespace);
    for resource in resources {
        let name = resource.name_any();
        api.patch(&name, &ssapply, &Patch::Apply(resource)).await?;
    }
    Ok(())
}
