use kube::Client;
use tonic::transport::Server;

use crate::grpc::api::entropy_server::EntropyServer;
use crate::grpc::api::middlewares_server::MiddlewaresServer;
use crate::grpc::api::node_server::NodeServer;
use crate::grpc::api::proxies_server::ProxiesServer;
use crate::grpc::api::services_server::ServicesServer;
use crate::grpc::api::tailscale_server::TailscaleServer;
use crate::grpc::api::upgrades_server::UpgradesServer;
use crate::grpc::api::users_server::UsersServer;

pub mod kubernetes;
pub mod manage;
pub mod sys_components;
pub mod utils;
pub mod grpc;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();
    #[cfg(feature = "dotenvy")]
    dotenvy::dotenv().expect("Failed to load .env file");
    let bind_address = std::env::var("BIND_ADDRESS").unwrap_or("[::]:8080".to_string());
    tracing::info!("Starting server on {}", bind_address);
    let address = bind_address.parse().unwrap();
    let nirvati_seed = std::env::var("NIRVATI_SEED").expect("NIRVATI_SEED not set");
    let socket_path = std::env::var("TAILSCALE_SOCKET").expect("TAILSCALE_SOCKET not set");
    let api_server = grpc::ApiServer {
        kube_client: Client::try_default()
            .await
            .expect("Failed to create kube client"),
        nirvati_seed: nirvati_seed.clone(),
        tailscale_socket: socket_path.clone(),
    };

    Server::builder()
        .add_service(EntropyServer::new(api_server.clone()))
        .add_service(MiddlewaresServer::new(api_server.clone()))
        .add_service(NodeServer::new(api_server.clone()))
        .add_service(UpgradesServer::new(api_server.clone()))
        .add_service(UsersServer::new(api_server.clone()))
        .add_service(ServicesServer::new(api_server.clone()))
        .add_service(ProxiesServer::new(api_server.clone()))
        .add_service(TailscaleServer::new(api_server))
        .serve(address)
        .await
        .expect("Failed to start server");
}
