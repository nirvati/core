use anyhow::Result;
use k8s_openapi::api::core::v1 as k8s;
use k8s_openapi::api::core::v1::Secret;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use kube::api::{DeleteParams, ListParams, PostParams};
use kube::{Api, Client};

use crate::kubernetes::apply::apply_with_ns;
use k8s_crds_traefik::middlewares::*;

pub async fn create_namespace(client: Client, namespace: &str) -> Result<()> {
    // Create the namespace if it doesn't exist
    let namespace_api: Api<k8s::Namespace> = Api::all(client.clone());
    let lp = ListParams {
        field_selector: Some(format!("metadata.name={}", namespace)),
        ..Default::default()
    };
    let currently_deployed_namespaces = namespace_api.list(&lp).await?;
    if currently_deployed_namespaces.items.is_empty() {
        let pp = PostParams::default();
        let namespace = k8s::Namespace {
            metadata: k8s_meta::ObjectMeta {
                name: Some(namespace.to_string()),
                ..Default::default()
            },
            ..Default::default()
        };
        namespace_api.create(&pp, &namespace).await?;
    }
    Ok(())
}

pub async fn delete_namespace(client: Client, namespace: &str) -> Result<()> {
    let namespace_api: Api<k8s::Namespace> = Api::all(client.clone());
    let dp = DeleteParams::default();
    namespace_api.delete(namespace, &dp).await?;
    Ok(())
}

pub async fn delete_auth_middleware(client: &Client, user: &str) -> Result<()> {
    let dp = DeleteParams::default();
    let api: Api<Middleware> = Api::namespaced(client.clone(), "nirvati");
    api.delete(&format!("auth-{}", user), &dp).await?;
    let api: Api<Secret> = Api::namespaced(client.clone(), "nirvati");
    api.delete(&format!("auth-{}", user), &dp).await?;
    Ok(())
}

pub async fn generate_auth_middleware(
    client: &Client,
    user_id: &str,
    password: &str,
) -> Result<()> {
    let (middleware, secret) = crate::manage::auth::generate_auth_middleware(user_id, password)?;
    let mw = vec![middleware];
    apply_with_ns(client.clone(), &mw, "nirvati").await?;
    let secret = vec![secret];
    apply_with_ns(client.clone(), &secret, "nirvati").await?;
    Ok(())
}

pub async fn get_node_ip(client: &Client) -> Result<String> {
    let api: Api<k8s::Node> = Api::all(client.clone());
    let lp = ListParams::default();
    let nodes = api.list(&lp).await?;
    // We are currently assuming 1 node only, so just get the first IP
    let node = nodes
        .items
        .into_iter()
        .next()
        .ok_or_else(|| anyhow::anyhow!("No nodes found!"))?;
    let addresses = node
        .status
        .ok_or(anyhow::anyhow!(
            "Node {} has no status!",
            node.metadata.name.clone().unwrap()
        ))?
        .addresses
        .ok_or(anyhow::anyhow!(
            "Node {} has no addresses!",
            node.metadata.name.clone().unwrap()
        ))?;
    let address = addresses.iter().find(|a| a.type_ == "InternalIP").unwrap();
    Ok(address.address.clone())
}
