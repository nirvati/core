use bcrypt::BcryptError;
use k8s_openapi::api::core::v1 as k8s;
use k8s_openapi::apimachinery::pkg::apis::meta::v1 as k8s_meta;
use k8s_openapi::ByteString;

use k8s_crds_traefik::middlewares as mw;

pub fn generate_auth_middleware(
    user_id: &str,
    password: &str,
) -> Result<(mw::Middleware, k8s::Secret), BcryptError> {
    let middleware = mw::Middleware {
        metadata: k8s_openapi::apimachinery::pkg::apis::meta::v1::ObjectMeta {
            name: Some(format!("auth-{}", user_id)),
            namespace: Some("nirvati".to_string()),
            ..Default::default()
        },
        spec: mw::MiddlewareSpec {
            basic_auth: Some(mw::MiddlewareBasicAuth {
                secret: Some(format!("auth-{}", user_id)),
                ..Default::default()
            }),
            ..Default::default()
        },
    };
    let secret = k8s::Secret {
        data: Some(
            vec![(
                "users".to_string(),
                ByteString(format!("{}:{}", user_id, bcrypt::hash(password, 14)?).into_bytes()),
            )]
            .into_iter()
            .collect(),
        ),
        metadata: k8s_meta::ObjectMeta {
            name: Some(format!("auth-{}", user_id)),
            namespace: Some("nirvati".to_string()),
            ..Default::default()
        },
        ..Default::default()
    };
    Ok((middleware, secret))
}
