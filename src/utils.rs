use hmac_sha256::HMAC;

pub fn derive_entropy(identifier: &str, nirvati_seed: &str) -> String {
    let mut hasher = HMAC::new(nirvati_seed);
    hasher.update(identifier.as_bytes());
    let result = hasher.finalize();
    hex::encode(result)
}
