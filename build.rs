pub fn main() {
    println!("cargo:rustc-link-arg=-latomic");
    tonic_build::configure()
        .protoc_arg("--experimental_allow_proto3_optional")
        .compile(&["protos/api.proto"], &["protos"])
        .unwrap();
}
