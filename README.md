# Nirvati Core

This is the core of Nirvati, responsible for managing all system components and apps. It exposes a gRPC API that other components (currently, only the GraphQL API) can interact with.
