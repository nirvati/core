syntax = "proto3";

package nirvati;

enum DNSProvider {
  Cloudflare = 0;
  NirvatiMe = 1;
}

message AddNirvatiDomainRequest {
  string user = 1;
  string domain = 2;
}

message DeleteNirvatiDomainRequest {
  string user = 1;
  string domain = 2;
}

message AddUserAuthRequest {
  string user = 1;
  string password = 2;
}

message DeleteUserAuthRequest {
  string user = 1;
}

message Release {
  map<string, string> notes = 1;
}

message Empty {}

message VersionInfo {
  string version = 1;
}

enum BuiltInACMEProvider {
  LetsEncrypt = 0;
  BuyPass = 1;
}

message ACMEProvider {
  oneof provider {
    BuiltInACMEProvider built_in = 1;
    string custom = 2;
  }
}

message TailscaleStatus {
  string authUrl = 1;
  bool authenticated = 2;
  optional string ownIp = 3;
}

message K3sVersion {
  bool installed = 1;
  optional string version = 2;
}

message NodeInfo {
  string ip = 1;
}

message EntropyRequest {
  string input = 1;
}

message EntropyResponse {
  string entropy = 1;
}

message AddUserNsRequest {
  string user = 1;
}

message DeleteUserNsRequest {
  string user = 1;
}

service Middlewares {
  rpc AddUserAuth(AddUserAuthRequest) returns (Empty) {}
  rpc RemoveUserAuth(DeleteUserAuthRequest) returns (Empty) {}
}

service Upgrades {
  rpc GetLonghornVersion(Empty) returns (VersionInfo) {}
  rpc GetLatestLonghornVersion(Empty) returns (VersionInfo) {}
  rpc UpdateLonghorn(VersionInfo) returns (Empty) {}

  rpc GetCertManagerVersion(Empty) returns (VersionInfo) {}
  rpc GetLatestCertManagerVersion(Empty) returns (VersionInfo) {}
  rpc UpdateCertManager(VersionInfo) returns (Empty) {}
  
  rpc GetSystemUpgradeControllerVersion(Empty) returns (VersionInfo) {}
  rpc GetLatestSystemUpgradeControllerVersion(Empty) returns (VersionInfo) {}
  rpc UpdateSystemUpgradeController(VersionInfo) returns (Empty) {}

  rpc GetK3sVersion(Empty) returns (K3sVersion) {}
  // These will only work if K3s is installed
  rpc GetLatestK3sVersion(Empty) returns (VersionInfo) {}
  rpc UpdateK3s(VersionInfo) returns (Empty) {}
}

service Node {
  rpc GetInfo(Empty) returns (NodeInfo) {}
}

service Entropy {
  rpc DeriveEntropy(EntropyRequest) returns (EntropyResponse) {}
}

service Tailscale {
  rpc GetStatus(Empty) returns (TailscaleStatus) {}
  rpc StartLogin(Empty) returns (Empty) {}
  rpc Start(Empty) returns (Empty) {}
}

service Users {
  rpc CreateUserNamespace(AddUserNsRequest) returns (Empty) {}
  rpc DeleteUserNamespace(DeleteUserNsRequest) returns (Empty) {}
}

message AddServiceRequest {
  oneof remote {
    string dns_name = 1;
    string ip = 2;
  }
  string namespace = 3;
  string name = 4;
  repeated uint32 tcp_ports = 5;
  repeated uint32 udp_ports = 6;
}

message RemoveServiceRequest {
  string namespace = 1;
  string name = 2;
}

service Services {
  rpc AddService(AddServiceRequest) returns (Empty) {}
  rpc RemoveService(RemoveServiceRequest) returns (Empty) {}
}

enum Protocol {
  HTTP = 0;
  HTTPS = 1;
}

message HttpsOptions {
  bool insecure_skip_verify = 1;
  optional string sni = 2;
}

message Route {
  string target_svc = 1;
  string remote_prefix = 2;
  string local_prefix = 3;
  bool enable_compression = 4;
  Protocol protocol = 5;
  // Technically a u16, but Kube-rs will take any int32
  int32 port = 6;
  optional HttpsOptions https = 7;
}

message AddProxyRequest {
  string namespace = 1;
  string domain = 2;
  repeated Route routes = 3;
}

message AddTcpProxyRequest {
  string namespace = 1;
  string domain = 2;
  string target_svc = 3;
  // Technically a u16, but Kube-rs will take any int32
  int32 port = 4;
  bool enable_redirect = 5;
}

message RemoveProxyRequest {
  string namespace = 1;
  string domain = 2;
}

service Proxies {
  rpc AddProxy(AddProxyRequest) returns (Empty) {}
  rpc AddTcpProxy(AddTcpProxyRequest) returns (Empty) {}
  rpc RemoveProxy(RemoveProxyRequest) returns (Empty) {}
}
